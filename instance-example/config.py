##########################################################
#                                                        #
#           Project Fixed valiables declared             #
#                                                        #
##########################################################
import os
## VARIABLE - SERVER DEPENDENT
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]='/root/template-to-pdf/instance/gcs-creds.json'  # On server

SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@IP:5432/db'  # Database credentials for creating conection
BUCKET = "gcs-bucket"  # GCS bucket
URL = "https://script.google.com/macros/s/AsdfsfsafgU1xGOPDekONzvIId-BRnsopuw3Tp1bww/exec?"  # URL of google app script for saksham samiksha

## CONSTANT - SERVER INDEPENDENT
SECRET_KEY = 'p9Bv<3Eidskfdk9w7834rufhksaf'  # Secret API key
SQLALCHEMY_TRACK_MODIFICATIONS = False
FLASK_CONFIG = 'production'  # Flask config (also present in app/__init__.py)
DIRPATH = "uploadFiles/"  # Directory for downloading the pdf's from drive
GOOGLECLOUDBASEURL = 'https://storage.googleapis.com/' + BUCKET + '/'  # GCS pdf base url
SESSIONCOOKIEBASEURL = 'http://url'  # Base url for getting session cookie
MAPPINGDETAILS = 'mappingDetails'  # name of mapping details sheet
OPTIONSSHEET = 'optionsSheet'  # name of options mapping sheet
ODKUSERNAME = 'username'  # Username of ODK aggregate (data streaming server)
ODKPASSWORD = 'password'  # Password of ODK aggregate server
